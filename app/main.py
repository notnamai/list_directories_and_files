"""Файл содержит главные узлы приложения."""

import uvicorn
from fastapi import FastAPI

from app.views import files_and_directories

app = FastAPI()
port = 54666

app.include_router(files_and_directories.router)


if __name__ == '__main__':
    uvicorn.run(app, host='0.1.0.0', port=port)
