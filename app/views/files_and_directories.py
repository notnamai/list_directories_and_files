"""Файл содержит эндпоинт списока файлов и директорий с датой их создания."""
import os

from fastapi import (
    APIRouter,
    Query,
    )

from app.config import root
from app.schemas.files_and_directories import (
    FileOrDir,
    ListFilesAndDirs,
    )

router = APIRouter(prefix='/api/meta')
query = Query()


@router.get('/', response_model=ListFilesAndDirs)
async def get_list_files_and_directories(path_dir: str = query) -> ListFilesAndDirs:
    """
    Функция принимает путь директории.

    :param path_dir: Путь директории
    :return: Возвращает список файлов и директорий с датой их создания которые находятся в указанной директории
    """
    cont = os.path.join(root, path_dir)

    if os.path.exists(cont):
        return ListFilesAndDirs(
            list_files_and_dirs=[FileOrDir(
                name=i_info,
                type='folder',
                time=int(os.path.getctime(os.path.join(cont, i_info),
                                          ),
                         ),
            )

                     for i_info in list(os.walk(cont))[0][1]
                 ] +
                 [FileOrDir(
                     name=i_info,
                     type='file',
                     time=int(os.path.getctime(os.path.join(cont, i_info),
                                               ),
                              ),
                 )
                     for i_info in list(os.walk(cont))[0][2]
                 ],
        )
    return ListFilesAndDirs(list_files_and_dirs=[])
