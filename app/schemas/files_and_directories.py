"""Файл схемы получения информации о файлах и папках."""

from typing import List

from pydantic import BaseModel


class FileOrDir(BaseModel):
    name: str
    type: str
    time: int


class ListFilesAndDirs(BaseModel):
    list_files_and_dirs: List[FileOrDir]
